// Modules to control application life and create native browser window
const { app, BrowserWindow, Menu } = require("electron");
const url = require('url') 
const path = require('path') 
const express = require('express')
const cors = require('cors')
const http = require('http')
const bodyParser = require('body-parser')
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const project = 'production' // production  development

var webapp = express();

// Add a simple route for static content served from 'public'
// webapp.use("/",express.static("web"));
webapp.use(express.static(__dirname + "/public"));
webapp.use(cors())
webapp.use(bodyParser.urlencoded({ extended: false }));
webapp.use(bodyParser.json());
webapp.use(require('./routes'));

// Create a server
var server = http.createServer(webapp);


function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    // width: 800,
    // height: 600,
    webPreferences: {
      preload: `${__dirname}/renderer.js`
    },
    frame: false,
    fullscreen : true
  });

  // and load the index.html of the app.
  mainWindow.loadURL(url.format ({ 
    pathname: path.join(__dirname + "public/", 'index.html'), 
    protocol: 'file:', 
    slashes: true 
  })) 

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on("closed", function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

function createMainMenu() {
  const template = [
    {
      label: "Lists",
      submenu: [
        {
          label: "Create new list",
          accelerator: "CommandOrControl+N",
          click() {
            mainWindow.webContents.send("create-list");
          }
        }
      ]
    }
  ];

  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
  createWindow();
  createMainMenu();
});

// Quit when all windows are closed.
app.on("window-all-closed", function() {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function() {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

app.on('browser-window-created',function(e, window) {
  server.listen(1111, "0.0.0.0", function () {
    if (project === 'development'){
      mainWindow.loadURL("http://192.168.2.1:" + 3001 + '/');
      mainWindow.webContents.openDevTools()
    }else {
      mainWindow.loadURL("http://localhost:" + 1111 + '/');
    }
  });
  window.setMenu(null);
});