const barrier = require('./barrier-gate')
const util = require('util');
const setTimeoutPromise = util.promisify(setTimeout);
var router = require('express').Router();
const CONFIG = require("./config.json")
const serialPath = CONFIG.BarrierGate

router.post("/left", function(req, res, next){
  barrier.connect(serialPath)
  .then( () => {
      console.log("\nOpening LEFT gate")
      return barrier.openLeft()
  })
  .then( (result) => { 
    return res.json({
      msg: 'OPENED',
      result: result
    });
  } )
  .catch( (result) => {
    return res.json({
      msg: 'error',
      result: result
    });
  } )
})

router.post("/right", function(req, res, next){
  barrier.connect(serialPath)
  .then( () => {
      console.log("\nOpening RIGHT gate")
      return barrier.openRight()
  })
  .then( (result) => { 
    return res.json({
      msg: 'OPENED',
      result: result
    });
  } )
  .catch( (result) => {
    return res.json({
      msg: 'error',
      result: result
    });
  } )
})
module.exports = router;



// barrier.connect(serialPath)
//   /* Testing: open left gate with automatically close */
//   .then( () => {
//       console.log("\nOpening LEFT gate")
//       return barrier.openLeft()
//   })
//   .then( (result) => { console.log("OPENED") } )
//   .catch( (result) => { console.log(result) } )
//   /* Testing: open right gate with automatically close */
//   .then( () => {
//       console.log("\nOpening RIGHT gate")
//       return barrier.openRight()
//   })
//   .then( (result) => { console.log("OPENED") } )
//   .catch( (result) => { console.log(result) } )
//   /* Testing: keep open left gate */
//   .then( () => {
//       console.log("\nKeep open LEFT gate")
//       return barrier.openLeft(true) 
//   })
//   .then( (result) => { console.log("OPENED") } )
//   .catch( (result) => { console.log(result) } )
//   /* Keep gate open for 10s */
//   .then( () => setTimeoutPromise(10000) )
//   /* Testing: close gate */
//   .then( () => {
//       console.log("\nClose gate")
//       return barrier.exitKeepOpen()
//   })
//   .then( (result) => { console.log("DONE") } )
//   .catch( (result) => { console.log(result) } )
//   .finally( () => barrier.disconnect() )