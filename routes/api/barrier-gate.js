
const SerialPort = require('serialport')

const BARRIER_DEFAULT_BUADRATE = 19200

const CMD_OPEN_GATE_LEFT       = Buffer.from("7E800001000080AA0001010095FC7E", 'hex')
const CMD_OPEN_GATE_RIGHT      = Buffer.from("7E800001000080AA00010200C0AF7E", 'hex')
const CMD_KEEP_OPEN_GATE_LEFT  = Buffer.from("7E800001000080AA0001010195FC7E", 'hex')
const CMD_KEEP_OPEN_GATE_RIGHT = Buffer.from("7E800001000080AA00010201C0AF7E", 'hex')
const CMD_KEEP_OPEN_EXIT       = Buffer.from("7E800001000083AA00018AAB7E", 'hex')
const CMD_READ_COUNTER         = Buffer.from("7E800001000023AA0001A37A7E", 'hex')
const CMD_CLEAR_COUNTER        = Buffer.from("7E800001000024AA0001694D7E", 'hex')

let serialInstance = undefined

const connect = (port) => {
    return new Promise( (resolve, reject) => {
        serialInstance = new SerialPort(port, { baudRate: BARRIER_DEFAULT_BUADRATE }, (err) => { reject(err) })
        serialInstance.on('open', () => { resolve(serialInstance) })
    })
}

const disconnect = () => {
    return serialInstance.close()
}

/**
 * Request command to barrier gate board
 * @param {Buffer} data - Read buffer
 * @param {function} cb - Callback function to process data with return 0 = skip, 1 = success and -1 fail
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 */
const writeCommand = (data, cb) => {
    return new Promise( (resolve, reject) => {
        const receivedCb = (readData) => {
            let done = cb(readData)
            if (done > 0) {
                clearTimeout(commandTimeout)
                serialInstance.removeListener('data', receivedCb)
                resolve({ result: "ok", data: readData })
            }
            else if (done < 0) {
                clearTimeout(commandTimeout)
                serialInstance.removeListener('data', receivedCb)
                reject({ result: "timeout", data: readData })
            }
        }

        const commandTimeout = setTimeout(() => {
            serialInstance.removeListener('data', receivedCb)
            reject({ result: "cmd-timeout", data: null })
        }, 8000)

        serialInstance.on('data', receivedCb)
        serialInstance.write(data)
    })
}

/**
 * To open arm of barrier to left side
 * @param {boolean} keepOpen
 * @return Promise
 */
const openLeft = (keepOpen = false) => {
    /** 
     * Write data success: 7e 80 00 01 00 00 80 00 00 01 ca ca 7e 00
     * Open left success: 7e 80 00 01 00 00 61 00 00 01 a3 7a 7e 00
     * Open left timeout: 7e 80 00 01 00 00 63 00 00 01 a3 7a 7e 00
     */
    const openLeftCb = (readData) => {
        let respCmd = readData.slice(6, 7);

        switch (respCmd.readUInt8()) {
            /* Open left success */
            case 0x61:
                return 1;

            /* Open left timeout */
            case 0x63:
                return -1;

            /* Open Command */
            case 0x80:
                if (keepOpen)
                    return 1;

            default:
                return 0;
        }
    }

    if (keepOpen)
        return writeCommand(CMD_KEEP_OPEN_GATE_LEFT, openLeftCb)
    else
        return writeCommand(CMD_OPEN_GATE_LEFT, openLeftCb)
}

/**
 * To open arm of barrier to right side
 * @param {boolean} keepOpen
 * @return Promise
 */
const openRight = (keepOpen = false) => {
    const openRightCb = (readData) => {
        let respCmd = readData.slice(6, 7);

        switch (respCmd.readUInt8()) {
            /* Open right success */
            case 0x62:
                return 1;

            /* Open right timeout */
            case 0x64:
                return -1;

            /* Open Command */
            case 0x80:
                if (keepOpen)
                    return 1;

            default:
                return 0;
        }
    }

    if (keepOpen)
        return writeCommand(CMD_KEEP_OPEN_GATE_RIGHT, openRightCb)
    else
        return writeCommand(CMD_OPEN_GATE_RIGHT, openRightCb)
}

/**
 * Close the arm keep open mode
 * @return Promise
 */
const exitKeepOpen = () => {
    return writeCommand(CMD_KEEP_OPEN_EXIT, (readData) => {
        let respCmd = readData.slice(6, 7);

        switch (respCmd.readUInt8()) {
            case 0x83:
                return 1;
        }
    })
}

/**
 * Read opening gate counter
 * @return Promise
 */
const readCounter = () => {
    return writeCommand(CMD_READ_COUNTER, (readData) => {
        let respCmd = readData.slice(6, 7);

        switch (respCmd.readUInt8()) {
            case 0x23:
                return 1;
        }
    })
}

/**
 * Clear opening gate counter
 * @return Promise
 */
const clearCounter = () => {
    return writeCommand(CMD_CLEAR_COUNTER, (readData) => {
        let respCmd = readData.slice(6, 7);

        switch (respCmd.readUInt8()) {
            case 0x24:
                return 1;
        }
    })
}

module.exports = {
    connect,
    disconnect,
    writeCommand,
    openLeft,
    openRight,
    exitKeepOpen,
    readCounter,
    clearCounter
}