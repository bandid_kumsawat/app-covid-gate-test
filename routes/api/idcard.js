var router = require('express').Router();
const CONFIG = require("./config.json")

const CMD = require('./command')
const CardReader = require('./scr35f-vcom')

var resss = [{
  fullname: "",
  idcard: ""
}]

CardReader.connect(CONFIG.CardReader, 9600)
  .then(() => {
    setInterval(function(){   
      CardReader.getStatus(CMD.READER_CARDSTATUS).then( (data) => {
        if (Buffer.compare(Buffer.from([0x00]), data.slice(1, 1+1)) === 0){
          CardReader.getStatus(CMD.READER_POWERON).then( (data) => {
            CardReader.selectCard().then((data) => {


              CardReader.setCommand(CMD.CMD_THFULLNAME).then((data) => {
                CardReader.requestData(0x64).then((data) => {
                  var buffchar = []
                  for (var i = 4;i < data.length;i++){
                    if (data[i] === 32){
                      break
                    }
                    buffchar.push(data[i])
                  }
                  resss[0].fullname = data[i]

                  // id
                  CardReader.setCommand(CMD.CMD_THFULLNAME).then((data) => {
                    CardReader.requestData(0x64).then((data) => {
                      var buffchar = []
                      for (var i = 4;i < data.length;i++){
                        if (data[i] === 32){
                          break
                        }
                        buffchar.push(data[i])
                      }
                      resss[0].idcard = data[i]
                      CardReader.getStatus(CMD.READER_POWEROFF).then((data) => {
                        if (Buffer.compare(Buffer.from([0x1B, 0x00, 0x00]), data.slice(0, 3)) === 0){
                          resss[0].idcard = ""
                          resss[0].fullname = ""
                        }
                      }) 
                    })
                  })
                })
              })

            })
          })
        }else {
          console.log(CMD.ISINSERTCARD.NOINSERTED)
        }
      })
    }, 2000)
  })

// router.post("/data", function(req, res, next){
//   return res.json({
//     msg: resss
//   });
// })

module.exports = router;
