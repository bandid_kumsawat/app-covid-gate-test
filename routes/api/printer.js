var router = require('express').Router();
var moment = require("moment")
// var appprinter = require("./AppPrinter")

var ErrorCauseStatus = require('./ErrorCauseStatus')
var OfflineCauseStatus = require('./OfflineCauseStatus')
var PrinterStatus = require('./PrinterStatus')
var RollPaperSensorStatus = require('./RollPaperSensorStatus')

router.post("/RollPaperSensorStatus", function(req, res, next){
  async function PrinterFunction(){
    var res_RollPaperSensorStatus = await appprinter.RollPaperSensorStatus()

    return res.json({res: res_RollPaperSensorStatus});
  }
  PrinterFunction()
  // return res.json({res: RollPaperSensorStatus});
})

router.post("/OfflineCauseStatus", function(req, res, next){
  async function PrinterFunction(){
    var res_OfflineCauseStatus = await appprinter.OfflineCauseStatus()

    return res.json({res: res_OfflineCauseStatus});
  }
  PrinterFunction()
  // return res.json({res: OfflineCauseStatus});
})

router.post("/ErrorCauseStatus", function(req, res, next){
  async function PrinterFunction(){
    var res_ErrorCauseStatus = await appprinter.ErrorCauseStatus()

    return res.json({res: res_ErrorCauseStatus});
  }
  PrinterFunction()
  // return res.json({res: ErrorCauseStatus});
})

router.post("/PrinterStatus", function(req, res, next){
  async function PrinterFunction(){
    var res_PrinterStatus = await appprinter.PrinterStatus()

    return res.json({res: res_PrinterStatus});
  }
  PrinterFunction()
  // return res.json({res: PrinterStatus});
})

router.post("/Print", function(req, res, next){
  var text = ''
  if(typeof req.body.text !== 'undefined'){
    text = req.body.text
  }
  async function PrinterFunction(){
    var restext = await appprinter.Print(text)

    return res.json({res: restext});
  }
  async function cut(){
    await appprinter.FullCut()
  }
  PrinterFunction()
  cut()

  // return res.json({res: true});
})

router.post("/FullCut", function(req, res, next){
  async function PrinterFunction(){
    var restext = await appprinter.FullCut()

    return res.json({res: restext});
  }
  PrinterFunction()

  // return res.json({res: true});
})

router.post("/PartialCut", function(req, res, next){
  async function PrinterFunction(){
    var restext = await appprinter.PartialCut()

    return res.json({res: restext});
  }
  PrinterFunction()

  // return res.json({res: true});
})

router.post("/PrintQr", function(req, res, next){
  var text = ''
  if(typeof req.body.text !== 'undefined'){
    text = req.body.text
  }
  async function PrinterFunction(){
    var restext = await appprinter.PrintQr(text)

    return res.json({res: restext});
  }
  async function cut(){
    await appprinter.FullCut()
  }
  PrinterFunction()
  cut()

  // return res.json({res: true});
})

router.post("/PrintBarcode", function(req, res, next){
  ('111222333444', 'EAN13', { width: 80, height: 100, position: 0, font: 'A'})
  var text = ''
  var type = ''
  var option = {}

  if(typeof req.body.text !== 'undefined'){
    text = req.body.text
  }
  if(typeof req.body.type !== 'undefined'){
    type = req.body.type
  }
  if(typeof req.body.option !== 'undefined'){
    option = req.body.option
  }

  async function PrinterFunction(){
    var restext = await appprinter.PrintBarcode(text, type, option)

    return res.json({res: restext});
  }
  async function cut(){
    await appprinter.FullCut()
  }
  PrinterFunction()
  cut()

  // return res.json({res: true});
})

module.exports = router;
