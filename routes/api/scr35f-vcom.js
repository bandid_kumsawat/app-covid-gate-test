
const SerialPort = require('serialport')

const CMD = require('./command')

const CARDREADER_DEFAULT_BUADRATE = 115200

let serialInstance = undefined

/**
 * 
 * Request command to barrier gate board
 * @param {Buffer} data - Read buffer
 * @return {Buffer}
 *            
 */
const checkSum = (Buffer) => {
  var result
  for (var i = 0;i < Buffer.length;i++){
    if (i === 0){
      result = Buffer[i] ^ 0
    }else {
      result = result ^ Buffer[i]
    }
  }
  return result
}

const connect = (port, baudrate) => {
    return new Promise( (resolve, reject) => {
        serialInstance = new SerialPort(port, { baudRate: baudrate ? baudrate : CARDREADER_DEFAULT_BUADRATE }, (err) => { reject(err) })
        serialInstance.on('open', () => { resolve(serialInstance) })
    })
}

const disconnect = () => {
  return new Promise( (resolve, reject) => {
    serialInstance.close()
    resolve()
  })
}


/**
 * Request command to barrier gate board
 * @param {Buffer} data - Read buffer
 * @param {function} cb - Callback function to process data with return 0 = skip, 1 = success and -1 fail
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 */
const writeCommand = (cmdBuffer, cb) => {
    serialInstance.on('data', cb)
    serialInstance.write(cmdBuffer)
}

/**
 * 
 * @param {any} cmd - Buffer command
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 * 
 */
const getStatus = (cmd) => {
    return new Promise( (resolve, reject) => {
        const openTimeout = setTimeout(() => {
          serialInstance.removeListener('data', receivedCb)
          reject({ result: "timeout", data: null })
        }, 8000)
        const receivedCb = (readData) => {
            let respCmd = readData
            serialInstance.removeListener('data', receivedCb)
            resolve(respCmd)
            clearTimeout(openTimeout)
        }
        writeCommand(cmd, receivedCb)
    })
}

/**
 * 
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 * 
 */

const selectCard = () => {
  var LLen = Buffer.from([Buffer.concat([CMD.SELECT, CMD.THAI_CARD]).length])
  var ss = Buffer.from(Buffer.concat([CMD.READER_HEAD, CMD.HLen, LLen, CMD.SELECT, CMD.THAI_CARD]))
  var Edc = Buffer.from([checkSum(ss)])
  var s1 = Buffer.from(Buffer.concat([ss, Edc]))
  return new Promise( (resolve, reject) => {
    const openTimeout = setTimeout(() => {
      serialInstance.removeListener('data', receivedCb)
      reject({ result: "timeout", data: null })
    }, 8000)
    const receivedCb = (readData) => {
        let respCmd = readData
        serialInstance.removeListener('data', receivedCb)
        resolve(respCmd)
        clearTimeout(openTimeout)
    }
    //console.log(s1)
    writeCommand(s1, receivedCb)
  })
}
/**
 * 
 * @param {any} cmd_pr - Buffer command
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 * 
 */
const setCommand = (cmd_pr) => {
  var LLen = Buffer.from([cmd_pr.length])
  var ss = Buffer.from(Buffer.concat([CMD.READER_HEAD, CMD.HLen, LLen, cmd_pr]))
  var Edc = Buffer.from([checkSum(ss)])
  var s2 = Buffer.from(Buffer.concat([ss, Edc]))
  return new Promise( (resolve, reject) => {
    const openTimeout = setTimeout(() => {
      serialInstance.removeListener('data', receivedCb)
      reject({ result: "timeout", data: null })
    }, 8000)
    const receivedCb = (readData) => {
        let respCmd = readData
        serialInstance.removeListener('data', receivedCb)
        resolve(respCmd)
        clearTimeout(openTimeout)
    }
    //console.log(s2)
    writeCommand(s2, receivedCb)
  })
}

/**
 * 
 * @param {any} registerLenth - length register for received
 * @return Promise with result obj
 *         {
 *             result: => (
 *                         "ok": Operation success,
 *                         "timeout": Operation timeout; For example arm does not move in 5s after sent command, board will response timeout, 
 *                         "cmd-timeout": Command does not response in 8s
 *                        )
 *             data: => Response of requested command
 *         }
 * 
 */
const requestData = (registerLenth) => {
  var REQ_ = Buffer.from([0x00, 0xC0, 0x00, 0x00, registerLenth]) // index 4 is length
  var LLen = Buffer.from([REQ_.length])
  var ss = Buffer.from(Buffer.concat([ CMD.READER_HEAD, CMD.HLen, LLen, REQ_ ]))
  var Edc = Buffer.from([checkSum(ss)])
  var s3 = Buffer.from(Buffer.concat([ss, Edc]))
  return new Promise( (resolve, reject) => {
    const openTimeout = setTimeout(() => {
      serialInstance.removeListener('data', receivedCb)
      reject({ result: "timeout", data: null })
    }, 8000)
    const receivedCb = (readData) => {
        let respCmd = readData
        serialInstance.removeListener('data', receivedCb)
        resolve(respCmd)
        clearTimeout(openTimeout)
    }
    //console.log(s3)
    writeCommand(s3, receivedCb)
  })
}

module.exports = {
  connect,
  disconnect,
  getStatus,
  selectCard,
  setCommand, 
  requestData
}

/**
 * 
 * รายละเอียดฟังก์ชัน
 * 
 * connect(port:String, baudrate:Int)  
 * @return serialInstance
 * 
 * disconnect(void)
 * @retrue void
 * 
 * getStatus(cmd:Buffer)
 * @retrue Buffer - Status ID Carder
 * 
 * selectCard(void)
 * @return void
 * 
 * setCommand(cmd_pr:Buffer)
 * @retrun Buffer - Response Command from ID Carder
 * 
 * requestData(registerLenth:Int)
 * @retrun Buffer - Response Data from ID Carder
 * 
 */